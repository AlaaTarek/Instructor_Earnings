from django.conf import settings
from django.db import models

# Backwards compatible settings.AUTH_USER_MODEL
USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class InstructorProfit(models.Model):

    current_course_id = models.CharField(verbose_name='Current Course ID', blank=True, null=True, max_length=400)
    number_of_users = models.IntegerField(verbose_name='Number Of Users', blank=True, null=True, max_length=400)
    course_price = models.FloatField(verbose_name='Course Price', blank=True, null=True, max_length=400)
    percentage_model = models.FloatField(verbose_name='Percentage Model', blank=True, null=True, max_length=400)
    course_profit = models.FloatField(verbose_name='CMC Username', blank=True, null=True, max_length=400)